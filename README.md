# CRON Jobs Ubuntu

Guide about how to enable cron jobs in ubuntu. I have checked this on ubuntu 16.04 and 18.04. 


# Go to the main root

Go to the root directory and type

`crontab -e`


# Write to File

Once you enter after writing a command, a file will be opened, add you schedule command like below

for example to run command every minute, for laravel project

`*/1 * * * * cd /var/www/proj && php artisan schedule:run`

# Example Images
https://gitlab.com/zeeshanAhmedOfficial/cron-jobs-ubuntu/blob/master/Screenshot_8.png

https://gitlab.com/zeeshanAhmedOfficial/cron-jobs-ubuntu/blob/master/Screenshot_9.png